extends Area2D

export (String) var sceneName = "Level 3"

func _on_LoseAreaLevel3_body_entered(body):
	if body.get_name() == "DroneSpace":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
