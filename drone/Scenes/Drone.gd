extends KinematicBody2D

export (int) var speed = 400
export (int) var GRAVITY = 800
export (int) var thrust_speed = 30

const UP = Vector2(0,-1)

var velocity = Vector2()

func get_input():
	velocity.x = 0
	if Input.is_action_pressed('ui_up'):
		velocity.y -= thrust_speed
	if Input.is_action_pressed('ui_right'):
		velocity.x += speed
		if $Sprite.rotation_degrees == 30:
			pass
		else:
			$Sprite.set_rotation_degrees($Sprite.rotation_degrees + 1)
	if Input.is_action_pressed('ui_left'):
		velocity.x -= speed
		if $Sprite.rotation_degrees == -30:
			pass
		else:
			$Sprite.set_rotation_degrees($Sprite.rotation_degrees - 1)
	if Input.is_action_just_released('ui_right'):
		$Sprite.set_rotation_degrees(0)
	if Input.is_action_just_released('ui_left'):
		$Sprite.set_rotation_degrees(0)
	if Input.is_action_just_pressed('back_to_main'):
		get_tree().change_scene(str("res://Scenes/Main Screen.tscn"))
func _physics_process(delta):
    velocity.y += delta * GRAVITY
    get_input()
    velocity = move_and_slide(velocity, UP)
