extends Area2D

export (String) var sceneName = "Level 2"

func _on_Win_Area_Trigger_body_entered(body):
    if body.get_name() == "Drone":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
