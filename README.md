# Game Jam

* Name	          : Jose Devian Hibono
* NPM	          : 1706039603
* Game Title      : DRONE-UP
* Itch.io Link    : [https://josedevian.itch.io/drone-up](https://josedevian.itch.io/drone-up)


Diversifier
1. Now you’re thinking with portals = Implementasikan teleportasi player
2. Mendaki Gunung Lewati Lembah = Implementasi lembah dan gunung di game
3. Utamakan Keselamatan = Player hanya punya satu nyawa dalam permainan


Fitur Wajib

- [x] Ada simulasi ​physics sebagai bagian dari ​gameplay (misal: objek pemain atau musuh memiliki ​node Rigidbody, ada perhitungan gaya gesek antara dua objek ketika bersentuhan, dsb.).
- [x] Ada ​user interface yang memberikan informasi mengenai ​state permainan (misal: nyawa pemain, peta, durasi permainan).
- [x] Ada menu awal permainan yang memberikan opsi kepada pemain untuk mulai bermain atau keluar dari permainan.
- [x] Ada e​nd condition ​(misal: w​in/lose​, ​time limit​)​ ​sebagai o​utcome​ dari permainan.
- [x] Ada minimal 1 level yang ​playable​ dari awal hingga akhir.
- [x] Ada minimal 1 tantangan (c​ hallenge​) yang harus diselesaikan oleh pemain dengan cara apapun agar dapat menyelesaikan o​bjective ​dalam permainan.
- [x] Ada minimal 1 objek (misal: objek geometri primitif, atau avatar karakter berbentuk manusia, dsb.) yang dikendalikan oleh pemain sebagai karakter di dalam game.


**Deskripsi Game:**

DRONE-UP is a 2D side-scrolling game where the main character is a drone.

The main goal of this game is to fly your drone from the take off point to the landing point. 
There are several challenges in this game, the character must not crash into anything but the take off point and the landing point.

The drone can be controlled using Arrow Button on your keyboard.

**Controls:**

No gravity controls (Level 3):
* Arrow Up = Move Up
* Arrow Down = Move Down
* Arrow Left = Move Left
* Arrow Right = Move Right

Gravity (Level 1 & 2):
* Arrow Up = Thrust
* Arrow Left = Move Left
* Arrow Right = Move Right

Created by Jose Devian Hibono for 1-Week Game Jam by CSUI Game Development Course

Some assets downloaded from:

* Spritesheets taken from kenneyplatformerpack
* Fonts taken from [dafont.com](http://dafont.com)
* Drone Character made by Nikita Golubev from [www.flaticon.com](https://www.flaticon.com/)
* Menu Screen Background taken from [https://anokolisa.itch.io/legacy-adventure-pack](https://anokolisa.itch.io/legacy-adventure-pack)

